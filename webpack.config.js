module.exports = {
    entry: "./client/js/main.js",
    output: {
        path: "./client/js/",
        filename: "bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loader: "babel",
                query: {compact: false}
            }
        ]
    }
};