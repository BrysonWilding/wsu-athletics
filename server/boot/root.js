var fs = require('fs');
var path = require("path");
module.exports = function (server) {

    //path to client dir
    var client = "../../client";

    var router = server.loopback.Router();
    //router.get('/', server.loopback.status());
    router.get('/css/:file', function (req, res) {
        var filepath = path.join(__dirname, client + "/css/" + req.params.file);
        fs.stat(filepath, function (err, status) {
            if (!err) {
                res.sendFile(filepath);
            } else {
                res.status(404).send("File not found");
            }
        });
    });
    router.get('/js/:file', function (req, res) {
        var filepath = path.join(__dirname, client + "/js/" + req.params.file);
        fs.stat(filepath, function (err, status) {
            if (!err) {
                res.sendFile(filepath);
            } else {
                res.status(404).send("File not found");
            }
        });
    });
    router.get('/', function (req, res) {
        //Serves main content
        res.sendFile("index.html", {root: path.join(__dirname, client)});
    });
    server.use(router);
};
