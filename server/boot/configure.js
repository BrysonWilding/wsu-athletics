module.exports = function (app) {
    var ds = app.dataSources['mongo'];
    ds.autoupdate();

    app.models.User.findOrCreate(
        {"email": "admin@admin.com"}, {
        "email": "admin@admin.com",
        "password": "admin"
    }, function(err, user){
        //TODO This runs before the dataconnector is initialized
        if (err) {
            console.log(err);
            return;
        }
        app.models.Role.create({
            name: 'admin'
        }, function(err, role) {
            if (err) return console.log(err);

            role.principals.create({
                principalType: app.models.RoleMapping.USER,
                principalId: user.UserID
            }, function(err, principal) {
                if (err) return console.log(err);
            });
        });
    })
};
