module.exports = function () {
    return function (req, res, next) {
        if (req.path == "/api/Users" && req.method == "POST" && req.body.email != "") {
            var app = req.app;
            app.models.Whitelist.findOne({where: {email: req.body.email}}, function(err, user){
                if (!user) {
                    res.send("FAIL: User not in whitelist");
                    return;
                } else {
                    next();
                }
            })
        } else {
            next();
        }
    }
};