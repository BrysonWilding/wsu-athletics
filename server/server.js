var loopback = require('loopback');
var boot = require('loopback-boot');
var bodyParser = require('body-parser');

var app = module.exports = loopback();

// boot scripts mount components like REST API
boot(app, __dirname);
app.use("/api/users", function (req, res, next) {
    console.log(req);
    app.models.whitelist.find(
        {
            where: {
                email: req.params.email
            }
        }, function (err, user) {
            if (err) {
                //fail method
                res.send("User is not whitelisted");
            } else {
                next();
            }
        });
});

app.start = function () {
    // start the web server
    return app.listen(process.env.PORT || 3000, function () {
        app.emit('started');
        var baseUrl = app.get('url').replace(/\/$/, '');
        console.log('Web server listening at: %s', baseUrl);
        if (app.get('loopback-component-explorer')) {
            var explorerPath = app.get('loopback-component-explorer').mountPath;
            console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
        }
    });
};

// start the server if `$ node server.js`
if (require.main === module) {
    app.start();
}
