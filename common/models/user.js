module.exports = function(User) {
    User.isStruggling = function(userid, cb){
        cb(null, User.survey(function (err, surveys){
            return surveys;
            })
        );
    };
    User.remoteMethod(
        'isStruggling',
        {
            accepts: {arg: 'UserID', type:'string'},
            returns: {arg: 'Surveys', type:'object'}
        }
    )
};
