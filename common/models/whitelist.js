module.exports = function(Whitelist) {
    Whitelist.bulkUpload = function(emails, cb){
        var error = false;
        for (var i = 0; i < emails.length; i++){
            Whitelist.create({email: emails[i]}, function(err, ems){
                if (err) {
                    console.log("Whitelist bulk upload failed");
                    error = true;
                }
            })
        }
        cb(null, !error);
    };
    Whitelist.remoteMethod(
        'bulkUpload',
        {
            accepts: {arg: 'Emails', type:'array'},
            returns: {arg: 'Accepted', type:'boolean'}
        }
    )
};
