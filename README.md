Weber State Athletics API
=========================

* NOTE: If you're familiar with Vagrant, there's a vagrantfile that will get you most the way going

Prerequisites
-------------
* Have Nodejs installed and in path
* Have NPM installed (Comes with nodejs, if on windows it came in the msi)


Getting Started Server-side
---------------------------
1. Get a database. The loopback connector works with the following:
* MongoDB - Which is what's currently used
* MySQL
* Oracle
* PostgreSQL
* SQL Server
The models are listed in /common. You may need additional passthrough models depending on your database. (Though these can be handled by loopack as well)
2. In /server/datasources.json add you database information.
<pre><code> { 
                connector: [mongodb, mysql, oracle, postgresql, mssql],
                database: [Database Name],
                debug: [True, False], // Logs transaction information,
                host: [hostname], // localhost if building local
                password: [Database Password],
                port: [Port to access DB],
                username: [User to connect to DB]
            }
</code></pre>
3. In /server/model-config.json you'll need to configure all objects to use the dataconnector you established in step 2
4. Install all dependencies by typing 'npm install'
5. From root project directory, run 'PORT <i>SomePort</i> node server/server.js'
6. Paste the address printed in the console in a web browser to view the app.

Getting started Client-Side
---------------------------

* Client-side is a reactjs application

1. Install Webpack 'npm install -g webpack'
2. Use 'npm run build' to build the js app, which is located in /client/js/bundle.js

Sidenote - Loopback is based on express, built by strongloop and then purchased by IBM. Shortly after 
the semester began, the creator of the project cut ties with IBM and strongloop. 
Then strongloop began spying on loopback users. https://github.com/strongloop/loopback/issues/1079
Needless to say, the project has not done well with the FOSS community since.

Getting the Phone App running
-----------------------------

The phone app is using phonegap to wrap the desktop experience into the app. You can create your own phonegap project
by taking contents of /client/css, /client/js/bundle.js, and /client/index_phonegap.html and placing them into your project's
/css, /js and index.html places respectively.

Notes
-----
* The documentation is fairly out of date. When creating a Remote-Hook, you're related objects aren't in model.relatedmodel. Contrary to what is shown, it's actually in model.app.models.relatedmodel.
* Middleware done "The express way" is run after all other routes, meaning it's useless for object manipulation
    * To fix this, create a module in /server/middleware that exports your function. Then reference it in middleware.json in the appropriate stage, before routes.
* You need to manually import the app to do any model manipulation in your bootscripts, a la require('../server');
    * Otherwise when debugging you'll notice you get a modelconstructor object instead of your DTO.
