var React = require('react');
var ReactRouter = require('react-router');
var ReactDOM = require('react-dom');
var Am = require('./athlete/am.js');
var Login = require('./login.js');
var Dashboard = require('./athlete/dashboard.js');
var PostWorkout = require('./athlete/post-workout.js');
var CreateAccount = require('./athlete/create-account.js');
var PleaseLogin = require('./please-login.js');
var EditAccount = require('./athlete/edit-account.js');
var Reports = require('./admin/reports.js');
var AdminDashboard = require('./admin/dashboard.js');
var EditTeams = require('./admin/edit-teams.js');
var AddAthletes = require('./admin/add-athletes.js');
var ForgotPassword = require('./forgot-password.js');
var EditAdminAccount = require('./admin/edit-account.js');
var helper = require('./rest-helper.js');

var Route = ReactRouter.Route;
var Router = ReactRouter.Router;
var IndexRoute = ReactRouter.IndexRoute;
var Link = ReactRouter.Link;

var Main = React.createClass({
    render: function () {
        return (
            <div className="container">
                <div className="page-header">
                    <h1>Wildcat Athlete Tracker</h1>
                </div>
                {this.props.children}
                <footer className="footer">
                    <p>&copy; Weber State University Athletics 2016</p>
                </footer>
            </div>
        )
    }
});

function requireAuth(nextState, replace) {
    if (localStorage.getItem("access_token") == null) {
        replace({
            pathname: '/please-login',
            state: {nextPathname: nextState.location.pathname}
        })
    }
    else if(localStorage.getItem("role") == "admin")
    {
        replace({
            pathname: '/admin/',
            state: {nextPathname: nextState.location.pathname}
        })
    }
}

//TODO make this check to see if user is admin
function requireAdminAuth(nextState, replace) {
    if (localStorage.getItem("access_token") == null) {
        replace({
            pathname: '/login',
            state: {nextPathname: nextState.location.pathname}
        })
    }
    else if (localStorage.getItem("role") != "admin") {
        replace({
            pathname: '/dashboard',
            state: {nextPathname: nextState.location.pathname}
        })
    }
}

ReactDOM.render(
    <Router>
        <Route path="/" component={Main}>
            <IndexRoute component={Dashboard} onEnter={requireAuth}/>
            <Route path="dashboard" component={Dashboard} onEnter={requireAuth}/>
            <Route path="am" component={Am} onEnter={requireAuth}/>
            <Route path="post-workout" component={PostWorkout} onEnter={requireAuth}/>
            <Route path="login" component={Login}/>
            <Route path="create-account" component={CreateAccount}/>
            <Route path="please-login" component={PleaseLogin}/>
            <Route path="edit-account" component={EditAccount} onEnter={requireAuth}/>
            <Route path="forgot-password" component={ForgotPassword}/>
        </Route>
        <Route path="/admin/" component={Main} onEnter={requireAdminAuth}>
            <IndexRoute component={AdminDashboard} onEnter={requireAdminAuth}/>
            <Route path="add-athletes" component={AddAthletes} onEnter={requireAdminAuth}/>
            <Route path="edit-account" component={EditAdminAccount} onEnter={requireAdminAuth}/>
            <Route path="edit-teams" component={EditTeams} onEnter={requireAdminAuth}/>
            <Route path="reports" component={Reports} onEnter={requireAdminAuth}/>
        </Route>
    </Router>,
    document.getElementById('app')
);