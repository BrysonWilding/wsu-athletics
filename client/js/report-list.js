var React = require('react');

var SurveyList = React.createClass({
    render: function() {
        var mContext = this.props.mContext;
        var data = mContext.state.surveys;
        var surveys = data.map(function(survey) {
            return (
                <option value={survey.SurveyId}>{survey.SurveyName}</option>
            )
        });
        return (
            <select onChange={mContext.handleSurvey} className="drop-down">
                {surveys}
            </select>
        );
    }
});

module.exports = SurveyList;