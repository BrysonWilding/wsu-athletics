//From https://vincentarelbundock.github.io/Rdatasets/datasets.html
var StudentDataChart = {
    columns : [
        {
            label : "day",
            type: "string"
        },
        {
            label : "Student Data",
            type: "number"
        }
    ],
    rows : [
        ["Monday",20],
        ["Tuesday",21],
        ["Wednesday",19],
        ["Thursday",25],
        ["Friday",17],
        ["Saturday",18]
    ]
};
module.exports = StudentDataChart;

