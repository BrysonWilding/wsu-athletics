var React = require('react');
var helper = require('./../rest-helper.js');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;

function checkPassword(str)
{
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
    return re.test(str);
}

var EditAccount = React.createClass({
    getInitialState: function () {
        return {email: "", email2: "", password: "", password2: ""};
    },
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    componentDidMount: function () {
        var mContext = this;
        helper.get("/users/" + localStorage.getItem("id"), mContext)
            .then(function (data) {
                mContext.setState(
                    {
                        email: data.username
                    }
                );
            });
    },
    handleEmail: function (e) {
        this.setState({email: e.target.value});
    },
    handleEmail2: function (e) {
        this.setState({email2: e.target.value});
    },
    handlePassword: function (e) {
        this.setState({password: e.target.value});
    },
    handlePassword2: function (e) {
        this.setState({password2: e.target.value});
    },
    attemptEditUser: function (e) {
        e.preventDefault();
        if (this.state.password != this.state.password2) {
            alert("Passwords don't match");
            return;
        }
        if (this.state.email != this.state.email2) {
            alert("Emails don't match");
            return;
        }
        if(!checkPassword(this.state.password)){
            alert("Password must have at least one number, one lowercase letter, one uppercase letter, and 6 characters.");
            return;
        }
        if(this.state.email.length == 0)
        {
            alert("Email is required");
            return;
        }

        var editedUser = {
            "password": helper.hash(this.state.password),
            "email": this.state.email,
            "username": this.state.email
        };

        var mContext = this;
        helper.put("/users/" + localStorage.getItem("id"), editedUser, mContext)
            .then(function (value) {
                mContext.context.router.push("/admin/");
            }, function (reason) {
                alert("Couldn't update user: " + reason.message); // Error!
                console.log("failed: " + reason.message + reason.error);
            });
    },
    render: function () {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-xs-6 col-sm-6 col-md-6">
                        <form className="form-horizontal" onSubmit={this.attemptEditUser}>
                            <fieldset>
                                <div id="legend">
                                    <legend className="white">Edit Account</legend>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="email">New Email</label>
                                    <div className="controls">
                                        <input type="text" id="email" name="email" placeholder="asdf@qwerty.com"
                                               className="form-control input-lg" value={this.state.email}
                                               onChange={this.handleEmail}/>
                                    </div>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="email2">Confirm New Email</label>
                                    <div className="controls">
                                        <input type="email" id="email2" name="email2" placeholder="asdf@qwerty.com"
                                               className="form-control input-lg" value={this.state.email2}
                                               onChange={this.handleEmail2}/>
                                    </div>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="password">New Password</label>
                                    <div className="controls">
                                        <input type="password" id="password" name="password" placeholder="*******"
                                               className="form-control input-lg" value={this.state.password}
                                               onChange={this.handlePassword}/>
                                    </div>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="password">Confirm New Password</label>
                                    <div className="controls">
                                        <input type="password" id="password" name="password" placeholder="*******"
                                               className="form-control input-lg" value={this.state.password2}
                                               onChange={this.handlePassword2}/>
                                    </div>
                                </div>
                                <br/>
                                <div className="row">
                                    <div className="col-xs-6 col-sm-6 col-md-6">
                                        <button className="btn btn-lg btn-success btn-block">Save</button>
                                    </div>
                                    <div className="col-xs-6 col-sm-6 col-md-6">
                                        <Link to="/admin/" className="btn btn-lg btn-primary btn-block">Cancel</Link>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = EditAccount;