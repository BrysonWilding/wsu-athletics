var React = require('react');
var helper = require('../rest-helper.js');
var ReactRouter = require('react-router');
var TeamList = require('../team-list.js');
var UserList = require('../user-list.js');
var SurveyList = require('../report-list.js');
var dateHelper = require('../date-helper.js');
var Link = ReactRouter.Link;
var DatePicker = require('react-datepicker');
var moment = require('moment');
//var LineChart = require('react-chartjs').Line;
var Chart = require('react-google-charts').Chart;
//var StudentData = require('../StudentData');
var CodeHolder = require('../CodeHolder');
var ss = require('simple-statistics');

var Reports = React.createClass({
    getInitialState: function () {

        //TODO this is how you can add a linear regression line
        var reg = ss.linearRegression([[0, 0], [1, 1], [2, 3]]);
        //y = mx + b
        console.log("m: " + reg.m + " b: " + reg.b);

        //TODO add in a row to the team members that says something like, "show all"
        //this option will make the scatter plot of all the athletes be shown instead
        // of the line graph. Something with linear regression would be pretty cool...
        return {
            reg: reg,
            startDate: moment(),
            teams: [],
            surveys: [],
            users: [],
            /*StudentDataChart: {
             rows:[],
             columns:[],
             chartType: ""
             },*/
            ReportChart: {
                rows: [],
                columns: [],
                chartType: ""
            },

            data: [],
            SurveyName: "AM",
            postWorkoutScores: [],
            amScores: [],
            heartRates: []
        };
    },
    contextTypes: {
     router: React.PropTypes.object.isRequired
     },
    //componentDidMount: function () {
    //    var mContext = this;
    //    helper.get("/Surveys?", mContext)
    //        .then(function (data) {
    //            for (var i = 0; i < data.length; i++) {
    //                mContext.setState({
    //                    data: data
    //                    //Sleep: data[i].Questions_Answers.Sleep,
    //                    //Fatigue: data[i].Questions_Answers.Fatigue,
    //                    //Stress: data[i].Questions_Answers.Stress,
    //                    //Mood: data[i].Questions_Answers.Mood,
    //                    //Soreness: data[i].Questions_Answers.Soreness
    //                });
    //                /*for(var j = 0; j < data.length; j++) {
    //                 console.log(data[j]);
    //
    //                 }*/
    //            }
    //
    //        });


        // console.log(this.state.Sleep);
        /* var StudentDataChart =  {
         rows : StudentData.rows,
         columns : StudentData.columns,
         options : {title: "AM Survey", hAxis: {title: 'Day', minValue: 1, maxValue: 25}, vAxis: {title: 'Survey Score'}},
         chartType : "LineChart",
         div_id: "StudentData"
         };*/
        //  console.log(this.state.HeartRate);
        //console.log(this.state.Sleep);
        //  console.log(this.state.Fatigue);
        //  console.log(this.state.Stress);
        //  console.log(this.state.Mood);
        //  console.log(this.state.Soreness);
        //  console.log(this.state.Sleep);
        ////  console.log(this.state.SurveyName);
        // var ReportChart = {
        //      rows : [
        //          ["HeartRate", this.state.HeartRate],
        //          ["Sleep", this.state.Sleep],
        //          ["Fatigue", this.state.Fatigue],
        //          ["Stress", this.state.Stress],
        //          ["Mood", this.state.Mood],
        //          ["Soreness", this.state.Soreness]
        //
        //      ] ,
        //      columns : [
        //          {
        //              label : "questions",
        //              type: "string"
        //          },
        //          {
        //              label : "Student Data",
        //              type: "number"
        //          }
        //      ],
        //      options : {title: "AM Survey", hAxis: {title: 'Day', minValue: 1, maxValue: 15}, vAxis: {title: 'Survey Score'}},
        //      chartType : "LineChart",
        //      div_id: "ReportData"
        //  };
        //  console.log(this.state.HeartRate);
        //  console.log(this.state.Sleep);
        //  console.log(this.state.Fatigue);
        //  console.log(this.state.Stress);
        //  console.log(this.state.Mood);
        //  console.log(this.state.Soreness);
        //  console.log(this.state.Sleep);
        //  //console.log(this.state.SurveyName);
        //  //var SurveyChart = {};
        //
        ////  var mContext = this;
        //  /*helper.get("/Teams?", mContext)
        //   .then(function (data) {
        //   mContext.setState({teams: data});
        //   });
        //   helper.get("/users?", mContext)
        //   .then(function (data) {
        //   mContext.setState({users: data});
        //   });
        //   helper.get("/Surveys?", mContext)
        //   .then(function (data) {
        //   mContext.setState({surveys: data});
        //   });*/
        //  this.setState({
        //     // 'StudentDataChart': StudentDataChart,
        //      'ReportChart': ReportChart,
        //
        //  });
        //  console.log(this.state.HeartRate);
        //  console.log(this.state.Sleep);
        //  console.log(this.state.Fatigue);
        //  console.log(this.state.Stress);
        //  console.log(this.state.Mood);
        //  console.log(this.state.Soreness);
        //  console.log(this.state.Sleep);
        //console.log(this.state.SurveyName);
    //},
    handleChange: function (date) {
        this.setState({
            startDate: date,
        });
    },
    handleTeam: function (e) {
        this.setState({team: e.target.value});
    },
    handleUser: function (e) {
        this.setState({user: e.target.value});
    },
    handleSurvey: function (e) {
        this.setState({survey: e.target.value});
    },
    handleHeartRate: function (e) {
        this.setState({HeartRate: e.target.value});
        //console.log(this.state.SurveyName);
    },
    handleSubmit: function (e) {
        var mContext = this;
        dateHelper.getSurveys(this.state.SurveyName, this.state.startDate, new Date(),
            function (data) {
                mContext.setState(
                    {
                        data: data,
                        SurveyName: data.SurveyName
                    }
                );

                var amScores = [];
                var heartRates = [];
                var postWorkoutScores = [];

                for (var i = 0; i < mContext.state.data.length; i++) {
                    var survey = mContext.state.data[i];
                    var day = survey.Questions_Answers;
                    if (survey.SurveyName == "AM") {
                        amScores.push({
                            "score": day.Sleep + day.Fatigue + day.Stress + day.Mood + day.Soreness,
                            "date": survey.createdAt,
                            "UserId": survey.UserId
                        });
                        heartRates.push({
                            "score": day.HeartRate,
                            "date": survey.createdAt,
                            "UserId": survey.UserId
                        });
                    }
                    else {
                        postWorkoutScores.push({
                            "score": day.Intensity * day.WorkoutTime,
                            "date": survey.createdAt,
                            "UserId": survey.UserId
                        })
                    }
                }

                mContext.setState(
                    {
                        amScores: amScores,
                        hearRates: heartRates,
                        postWorkoutScores: postWorkoutScores
                    }
                )

            }, function (reason) {
                console.log("error: " + reason.message + reason.error);
            });
    },
    render: function () {
        var mContext = this;

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-4">
                        <label className="control-label" htmlFor="teamId">Team</label>
                        <div className="controls">
                            <TeamList mContext={this}/>
                        </div>
                        <label className="control-label" htmlFor="userId">Team Members</label>
                        <div className="controls">
                            <UserList mContext={this}/>
                        </div>
                        <label className="control-label" htmlFor="surveyId">Survey</label>
                        <div className="controls">
                            <SurveyList mContext={this}/>
                        </div>
                        <label className="control-label" htmlFor="date">Date Range</label>
                        <div className="controls">
                            <DatePicker
                                className="date-black-font"
                                selected={this.state.startDate}
                                maxDate={moment()}
                            />
                        </div>
                    </div>
                    <div className="col-md-8">
                        <h3> Student Data </h3>
                        <Chart chartType={this.state.ReportChart.chartType} width={"500px"} height={"300px"}
                               rows={this.state.ReportChart.rows} columns={this.state.ReportChart.columns}
                               options={this.state.ReportChart.options} graph_id={this.state.ReportChart.div_id}/>
                    </div>
                    <div className="col-xs-6 col-sm-6 col-md-6">
                        <Link to="/admin/" className="btn btn-primary">Back</Link>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = Reports;