var React = require('react');
var helper = require('../rest-helper.js');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;

var AddAthletes = React.createClass({
    getInitialState: function () {
        return {emails: ""};
    },
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    handleEmails: function (e) {
        this.setState({emails: e.target.value});
    },
    attemptWhiteList: function (e) {
        e.preventDefault();
        //var emailArray = this.state.emails.trim.split(","); //TODO check for @ and .?
        var emailArray = this.state.emails.replace(/ /g, '').split(",");

        //var athleteEmails= [];
        //
        //for(var i = 0; i < emailArray.length; i++)
        //{
        //    athleteEmails[i] = {"email": emailArray[i]};
        //    console.log(athleteEmails[i]);
        //}

        var mContext = this;
        helper.post("/Whitelists/bulkUpload", emailArray)
            .then(function (value) {
                console.log("emails whitelisted: " + value);
                mContext.context.router.push("/admin/");
            }, function (reason) {
                console.log("couldn't whitelist emails: " + reason.status + " " + reason.responseText); // Error!
            });
    },
    render: function () {
        return (
            <div className = "container">
                <form className="form-horizontal" onSubmit={this.attemptWhiteList}>
                    <div id="legend">
                        <legend className="white">Add emails to whitelist</legend>
                    </div>
                    <div className="control-group">
                        <label className="control-label" htmlFor="emails">Emails</label>
                        <div className="controls">
                            <textarea
                                   id="emails"
                                   rows="10"
                                   name="emails"
                                   placeholder="bruhseph@msn.com, broseph@hotmail.com, ..."
                                   className="form-control input-lg"
                                   value = {this.state.emails} onChange = {this.handleEmails} />
                        </div>
                    </div>
                    <br/>
                    <div className="row">
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <input type="submit" className="btn btn-lg btn-success btn-block" value="Submit"/>
                        </div>
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <Link to="/admin/" className="btn btn-lg btn-primary btn-block">Cancel</Link>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
});

module.exports = AddAthletes;