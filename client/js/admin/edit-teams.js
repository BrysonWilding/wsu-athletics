var React = require('react');
var helper = require('./../rest-helper.js');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;

var EditTeams = React.createClass({
    getInitialState: function () {
        return {data: [], newTeam: ""};
    },
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    componentDidMount: function () {
        var teams = this;
        helper.get("/Teams", teams)
            .then(function (data) {
                for (var i = 0; i < data.length; i++) {
                    teams.setState({data: data});
                }
            });
    },
    handleNewTeam: function (e) {
        this.setState({newTeam: e.target.value});
    },
    attemptCreateTeam: function (e) {
        e.preventDefault();

        if(this.state.newTeam.length < 3) {
            alert("Team name must be larger then 3 characters!");
            return;
        }

        var mNewTeam = {
            "Name": this.state.newTeam
            //"id":"14",
            //"userId": "15"
        };

        var mContext = this;

        helper.post("/Teams", mNewTeam, mContext)
            .then(function (value) {
                console.log("Success!");
                mContext.reload();
            });
    },
    attemptDeleteTeam: function (e) {
        console.log("deleting team: " + e.target.value);

        var r = confirm("Are you sure you want to delete this team?");
        if (r == true) {
            var mContext = this;
            helper.delete("/Teams/" + e.target.value, mContext)
                .then(function (value) {
                    console.log("Success!");
                    mContext.reload();
                });
        }
    },
    attemptSaveTeam: function (e) {
        console.log("saving: " + e.target.value);
        var mTeam = this.state.data[e.target.value];

        var mContext = this;
        helper.put("/Teams/" + mTeam.id, mTeam, mContext)
            .then(function (value) {
                console.log("Success!");
                mContext.reload();
            });
    },
    reload: function () {
        //janky fix. There didn't seem to be a good one anywhere I saw
        this.context.router.replace("/admin/");
        this.context.router.replace("/admin/edit-teams");
    },
    render: function () {

        var mContext = this;

        function handleTeamName(i, e) {
            //console.log("e: " + e.target.value + " i: " + i);
            var temp = mContext.state.data;
            temp[i].Name = e.target.value;
            mContext.setState({data: temp});
        }

        var teams = [];
        var data = this.state.data;
        for (var i = 0; i < data.length; i++) {
            var team = data[i];
            teams[i] = (
                <li className="list-group-item edit-black-back" key={i + "d"}>
                    <input key={i + "i"} type="text" value={team.Name} onChange={handleTeamName.bind(this, i)}/>
                    <div className="btn-group btn-flt-right">
                      <button className="btn btn-success" key={i + "b1"} value={i} onClick={mContext.attemptSaveTeam}>Save</button>
                      <button className="btn btn-danger" key={i + "b2"} value={team.id} onClick={mContext.attemptDeleteTeam}>Delete</button>
                    </div>
                </li>

            );
        }

        return (
            <div className="container">
                <div className="row">
                    <div className="col-xs-6 col-sm-6 col-md-6">
                        <h3>Team List</h3>
                        <ul className="list-group">{teams}</ul>
                        <div>
                            <input type="text"
                                   placeholder="Team name"
                                   value={this.state.newTeam}
                                   onChange={this.handleNewTeam}/>
                            <button className="btn btn-success"
                                onClick={this.attemptCreateTeam}>
                                Add New Team
                            </button>
                        </div>
                        <br/>
                            <Link to="/admin/" className="btn btn-primary">Cancel</Link>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = EditTeams;