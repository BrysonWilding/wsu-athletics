module.exports = {
    getDay: function (offset) {
        //return new Date(new Date().setDate(new Date().setHours(0,0,0,0).getDate()-offset));
        var d = new Date();
        d.setDate(d.getDate()+offset);
        d.setHours(0,0,0,0);
        return new Date(d);
    },
    getSurveys: function (beginDate, endDate, success, error) {
        helper.get("/Surveys?filter[where][createdAt][gt]=" + beginDate
                + "&filter[where][createdAt][lt]=" + endDate
                + "&filter[order]=createdAt  ASC")
            .then(success, error);
    }
};