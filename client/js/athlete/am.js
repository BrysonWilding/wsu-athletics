var React = require('react');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;
var helper = require('./../rest-helper.js');
var date = require('./../date-helper.js');

//TODO style it up
var sleepOptions = ["Insomnia", "Restless sleep", "Difficulty falling asleep", "Good", "Very restful"];
var fatigueOptions = ["Always tired", "More tired than normal", "Normal", "Fresh", "Very fresh"];
var sorenessOptions = ["Very sore", "Increase in soreness/tightness", "Normal", "Feeling good", "Feeling great"];
var moodOptions = ["Highly annoyed/irritable/down", "Aggravated/short tempered", "Less interested in others or activities than usual",
    "Generally good mood", "Very positive mood"];
var stressOptions = ["Highly stressed", "Feeling stressed", "Normal", "Relaxed", "Very relaxed"];

var SURVEY_NAME = "AM";
var completed = ["not complete", "completed"];


var Am = React.createClass({
    getInitialState: function () {
        return {
            heartRate: 70,
            sleep: 2,
            fatigue: 2,
            stress: 2,
            mood: 2,
            soreness: 2,
            yesterdayComplete: 0,
            todayComplete: 0,
            day: 0,
            todaySurvey: {},
            yesterdaySurvey: {}
        };
    },
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    componentDidMount: function () {
        var mContext = this;
        helper.get("/Surveys/findOne?filter[where][UserId]=" + localStorage.getItem("id")
                + "&filter[where][createdAt][gt]=" + date.getDay(0)
                + "&filter[where][SurveyName]=" + SURVEY_NAME + "&filter[order]=createdAt  ASC")
            .then(function (data) {
                if (data != {}) {
                    var survey = data;
                    mContext.setState(
                        {
                            todaySurvey: data,
                            heartRate: survey.Questions_Answers.HeartRate,
                            sleep: survey.Questions_Answers.Sleep,
                            fatigue: survey.Questions_Answers.Fatigue,
                            stress: survey.Questions_Answers.Stress,
                            mood: survey.Questions_Answers.Mood,
                            soreness: survey.Questions_Answers.Soreness,
                            todayComplete: 1
                        }
                    );
                }
            });
        helper.get("/Surveys/findOne?filter[where][UserId]=" + localStorage.getItem("id")
                + "&filter[where][createdAt][gt]=" + date.getDay(-1)
                + "&filter[where][createdAt][lt]=" + date.getDay(0)
                + "&filter[where][SurveyName]=" + SURVEY_NAME + "&filter[order]=createdAt  ASC")
            .then(function (data) {
                if (data != {}) {
                    mContext.setState(
                        {
                            yesterdaySurvey: data,
                            yesterdayComplete: 1
                        }
                    );
                }
            });
    },
    handleHeartRate: function (e) {
        this.setState({heartRate: e.target.value});
    },
    handleSleep: function (e) {
        this.setState({sleep: e.target.value});
    },
    handleFatigue: function (e) {
        this.setState({fatigue: e.target.value});
    },
    handleStress: function (e) {
        this.setState({stress: e.target.value});
    },
    handleMood: function (e) {
        this.setState({mood: e.target.value});
    },
    handleSoreness: function (e) {
        this.setState({soreness: e.target.value});
    },
    handleDay: function (e) {
        var survey;
        if (e.target.value == 0)
            survey = this.state.todaySurvey;
        else
            survey = this.state.yesterdaySurvey;

        if (survey.Questions_Answers == null)
            this.setState(
                {
                    day: e.target.value
                }
            );
        else
            this.setState(
                {
                    day: e.target.value,
                    heartRate: survey.Questions_Answers.HeartRate,
                    sleep: survey.Questions_Answers.Sleep,
                    fatigue: survey.Questions_Answers.Fatigue,
                    stress: survey.Questions_Answers.Stress,
                    mood: survey.Questions_Answers.Mood,
                    soreness: survey.Questions_Answers.Soreness
                }
            );

    },
    submitSurvey: function (e) {
        e.preventDefault();
        var oldSurvey;
        var date;
        var survey;
        var mContext = this;
        if (mContext.state.day == 0) {
            oldSurvey = mContext.state.todaySurvey;
            date = new Date();
        }
        else {
            oldSurvey = mContext.state.yesterdaySurvey;
            date = new Date(new Date().setDate(new Date().getDate() - 1));
        }

        if (oldSurvey == {}) {
            survey =
            {
                "SurveyName": SURVEY_NAME,
                "Questions_Answers": {
                    "HeartRate": mContext.state.heartRate,
                    "Sleep": mContext.state.sleep,
                    "Fatigue": mContext.state.fatigue,
                    "Stress": mContext.state.stress,
                    "Mood": mContext.state.mood,
                    "Soreness": mContext.state.soreness
                },
                "createdAt": date,
                "UserId": localStorage.getItem("id")
            };

            helper.post("/Surveys", survey)
                .then(function (value) {
                        console.log("updating survey");
                        mContext.context.router.push("/dashboard");
                    }
                    , function (reason) {
                        alert("Submit survey failed: " + reason.message);
                        console.log("failed: " + reason.message + reason.error);
                    });
        }
        else {
            survey =
            {
                "SurveyName": SURVEY_NAME,
                "Questions_Answers": {
                    "HeartRate": mContext.state.heartRate,
                    "Sleep": mContext.state.sleep,
                    "Fatigue": mContext.state.fatigue,
                    "Stress": mContext.state.stress,
                    "Mood": mContext.state.mood,
                    "Soreness": mContext.state.soreness
                },
                "UserId": localStorage.getItem("id")
            };

            helper.put("/Surveys/" + oldSurvey.SurveyId, survey)
                .then(function (value) {
                        mContext.context.router.push("/dashboard");
                        console.log("updating survey");
                    }
                    , function (reason) {
                        alert("Submit survey failed: " + reason.message);
                        console.log("failed update: " + reason.message + reason.error);
                    });
        }
    },
    render: function () {
        return (
            <div className="container">
                <form className="form-signin" onSubmit={this.submitSurvey}>
                    <h3>AM Survey</h3>
                    <div>
                        <label htmlFor="day">Day</label>
                        <br/>
                        <select className="drop-down" onChange={this.handleDay}>
                            <option value={0}>Today ({completed[this.state.todayComplete]})</option>
                            <option value={1}>Yesterday ({completed[this.state.yesterdayComplete]})</option>
                        </select>
                    </div>

                    <label htmlFor="heartRate">Resting Heart Rate</label>
                    <input value={this.state.heartRate} onChange={this.handleHeartRate} type="number" id="heartRate"
                           className="form-control" placeholder="Heart Rate" min="10" max="160" required=""
                           autofocus=""/>
                    <br/>
                    <label htmlFor="sleep">Quality of Sleep: {sleepOptions[this.state.sleep]}</label>
                    <input value={this.state.sleep} onChange={this.handleSleep} type="range" min="0" max="4"
                           id="sleep"/>
                    <br/>
                    <label htmlFor="fatigue">Fatigue: {fatigueOptions[this.state.fatigue]}</label>
                    <input value={this.state.fatigue} onChange={this.handleFatigue} type="range" min="0" max="4"
                           id="fatigue"/>
                    <br/>
                    <label htmlFor="stress">Stress: {stressOptions[this.state.stress]}</label>
                    <input value={this.state.stress} onChange={this.handleStress} type="range" min="0" max="4"
                           id="stress"/>
                    <br/>
                    <label htmlFor="mood">Mood: {moodOptions[this.state.mood]}</label>
                    <input value={this.state.mood} onChange={this.handleMood} type="range" min="0" max="4" id="mood"/>
                    <br/>
                    <label htmlFor="soreness">Soreness: {sorenessOptions[this.state.soreness]}</label>
                    <input value={this.state.soreness} onChange={this.handleSoreness} type="range" min="0" max="4"
                           id="soreness"/>
                    <br/>
                    <div className="row">
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <input type="submit" className="btn btn-lg btn-success btn-block" value="Submit"/>
                        </div>
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <Link to="/dashboard" className="btn btn-lg btn-primary btn-block">Cancel</Link>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
});

module.exports = Am;