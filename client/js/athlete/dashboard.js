var React = require('react');
var ReactRouter = require('react-router');
var helper = require('./../rest-helper.js');
var Link = ReactRouter.Link;

var Dashboard = React.createClass({
    signOut: function () {
        helper.post("/users/logout")
            .then(function () {
                console.log("Logged out"); // Success! Logged out!
                localStorage.clear();
            }, function (reason) {
                console.log("couldn't log out: " + reason.status + " " + reason.responseText); // Error!
                localStorage.clear();
            });
    },
    render: function () {
        return (
            <div className="container">
                <div className="row vertical-center-row">
                    <div className="col-xs-12 col-sm-6 col-md-8">
                        <div className="btn-group-vertical">
                            <p><Link to="/am">
                                <button type="button" className="btn btn-primary btn-block dashboard-btn">AM Survey</button>
                            </Link></p>
                            <p><Link to="/post-workout">
                                <button type="button" className="btn btn-primary btn-block dashboard-btn">Post Practice/Training<br/>Session Survey</button>
                            </Link></p>
                            <p><Link to="/edit-account">
                                <button type="button" className="btn btn-primary btn-block dashboard-btn">Edit Account</button>
                            </Link></p>
                            <p><Link to="/login">
                                <button type="button" className="btn btn-primary btn-block dashboard-btn" onClick={this.signOut}>Sign Out</button>
                            </Link></p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = Dashboard;