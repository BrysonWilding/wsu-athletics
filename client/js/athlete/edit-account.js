var React = require('react');
var helper = require('./../rest-helper.js');
var ReactRouter = require('react-router');
var TeamList = require('../team-list.js');
var Link = ReactRouter.Link;

function checkPassword(str)
{
    var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
    return re.test(str);
}

var EditAccount = React.createClass({
    getInitialState: function () {
        return {firstName: "", lastName: "", teamId: "", password: "", password2: "", teams: []};
    },
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    componentDidMount: function () {
        var mContext = this;
        helper.get("/users/" + localStorage.getItem("id"), mContext)
            .then(function (data) {
                mContext.setState(
                    {
                        firstName: data.FirstName,
                        lastName: data.LastName,
                        teamId: data.teamId
                    }
                );
            });

        helper.get("/Teams?", mContext)
            .then(function (data) {
                mContext.setState({teams: data});
            });
    },
    handleFirstName: function (e) {
        this.setState({firstName: e.target.value});
    },
    handleLastName: function (e) {
        this.setState({lastName: e.target.value});
    },
    handleTeam: function (e) {
        this.setState({teamId: e.target.value});
    },
    handlePassword: function (e) {
        this.setState({password: e.target.value});
    },
    handlePassword2: function (e) {
        this.setState({password2: e.target.value});
    },
    attemptEditUser: function (e) {
        e.preventDefault();
        if(!checkPassword(this.state.password)){
            alert("Password must have at least one number, one lowercase letter, one uppercase letter, and 6 characters.");
            return;
        }
        if (this.state.password != this.state.password2) {
            alert("Passwords don't match");
            return;
        }
        if (this.state.firstName.length < 1)
        {
            alert("First name is required");
            return;
        }
        if (this.state.lastName.length < 1)
        {
            alert("Last name is required");
            return;
        }
        if (this.state.teamId.length < 1) {
            alert("Please select a team");
            return;
        }

        var editedUser = {
            "password": helper.hash(this.state.password),
            "FirstName": this.state.firstName,
            "LastName": this.state.lastName,
            "teamId": this.state.teamId
        };

        var mContext = this;
        helper.put("/users/" + localStorage.getItem("id"), editedUser, mContext)
            .then(function (value) {
                mContext.context.router.push("/dashboard");
            }, function (reason) {
                alert("Couldn't update user: " + reason.message); // Error!
                console.log("failed: " + reason.message + reason.error);
            });
    },
    render: function () {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-xs-6 col-sm-6 col-md-6">
                        <form className="form-horizontal" onSubmit={this.attemptEditUser}>
                            <fieldset>
                                <div id="legend">
                                    <legend className="white">Edit Account</legend>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="teamId">Team</label>
                                    <div className="controls">
                                        <TeamList mContext={this}/>
                                    </div>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="firstname">First Name</label>
                                    <div className="controls">
                                        <input type="text" id="firstname" name="firstname" placeholder="John"
                                               className="form-control input-lg" value={this.state.firstName}
                                               onChange={this.handleFirstName}/>
                                    </div>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="lastname">Last Name</label>
                                    <div className="controls">
                                        <input type="text" id="lastname" name="lastname" placeholder="Smith"
                                               className="form-control input-lg" value={this.state.lastName}
                                               onChange={this.handleLastName}/>
                                    </div>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="password">New Password</label>
                                    <div className="controls">
                                        <input type="password" id="password" name="password" placeholder="*******"
                                               className="form-control input-lg" value={this.state.password}
                                               onChange={this.handlePassword}/>
                                    </div>
                                </div>
                                <div className="control-group">
                                    <label className="control-label" htmlFor="password">Confirm New Password</label>
                                    <div className="controls">
                                        <input type="password" id="password" name="password" placeholder="*******"
                                               className="form-control input-lg" value={this.state.password2}
                                               onChange={this.handlePassword2}/>
                                    </div>
                                </div>
                                <br/>
                                <div className="row">
                                    <div className="col-xs-6 col-sm-6 col-md-6">
                                        <button className="btn btn-lg btn-success btn-block">Save</button>
                                    </div>
                                    <div className="col-xs-6 col-sm-6 col-md-6">
                                        <Link to="/dashboard" className="btn btn-lg btn-primary btn-block">Cancel</Link>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = EditAccount;