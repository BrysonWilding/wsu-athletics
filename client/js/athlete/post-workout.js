var React = require('react');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;
var helper = require('./../rest-helper.js');
var date = require('./../date-helper.js');


var completed = ["not complete", "completed"];
var SURVEY_NAME = "PostWorkout";

var PostWorkout = React.createClass({
    getInitialState: function () {
        return {
            workoutTime: 60,
            intensity: 5,
            yesterdayComplete: 0,
            todayComplete: 0,
            day: 0,
            todaySurvey: {},
            yesterdaySurvey: {}
        };
    },
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    componentDidMount: function () {
        var mContext = this;
        helper.get("/Surveys/findOne?filter[where][UserId]=" + localStorage.getItem("id")
                + "&filter[where][createdAt][gt]=" + date.getDay(0)
                + "&filter[where][SurveyName]=" + SURVEY_NAME + "&filter[order]=createdAt  ASC")
            .then(function (data) {
                if (data != {}) {
                    var survey = data;
                    mContext.setState(
                        {
                            todaySurvey: data,
                            workoutTime: survey.Questions_Answers.WorkoutTime,
                            intensity: survey.Questions_Answers.Intensity,
                            todayComplete: 1
                        }
                    );
                }
            });
        helper.get("/Surveys/findOne?filter[where][UserId]=" + localStorage.getItem("id")
                + "&filter[where][createdAt][gt]=" + date.getDay(-1)
                + "&filter[where][createdAt][lt]=" + date.getDay(0)
                + "&filter[where][SurveyName]=" + SURVEY_NAME + "&filter[order]=createdAt  ASC")
            .then(function (data) {
                if (data != {}) {
                    mContext.setState(
                        {
                            yesterdaySurvey: data,
                            yesterdayComplete: 1
                        }
                    );
                }
            });
    },
    handleWorkoutTime: function (e) {
        this.setState({workoutTime: e.target.value});
    },
    handleIntensity: function (e) {
        this.setState({intensity: e.target.value});
    },
    handleDay: function (e) {
        var survey;
        if(e.target.value == 0)
            survey = this.state.todaySurvey;
        else
            survey = this.state.yesterdaySurvey;

        if(survey.Questions_Answers == null) {
            this.setState(
                {
                    day: e.target.value
                }
            );
        }
        else {
            this.setState(
                {
                    day: e.target.value,
                    workoutTime: survey.Questions_Answers.WorkoutTime,
                    intensity: survey.Questions_Answers.Intensity
                }
            );
        }
    },
    submitSurvey: function (e) {
        e.preventDefault();
        var oldSurvey;
        var date;
        var survey;
        var mContext = this;
        if(mContext.state.day == 0) {
            oldSurvey = mContext.state.todaySurvey;
            date = new Date();
        }
        else
        {
            oldSurvey = mContext.state.yesterdaySurvey;
            date = new Date(new Date().setDate(new Date().getDate() - 1));
        }

        if(oldSurvey == {}) {
            survey =
            {
                "SurveyName": SURVEY_NAME,
                "Questions_Answers": {
                    "Intensity": mContext.state.intensity,
                    "WorkoutTime": mContext.state.workoutTime
                },
                "createdAt": date,
                "UserId": localStorage.getItem("id")
            };

            helper.post("/Surveys", survey)
                .then(function (value) {
                        mContext.context.router.push("/dashboard");
                    }
                    , function (reason) {
                        alert("Submit survey failed: " + reason.message);
                        console.log("failed: " + reason.message + reason.error);
                    });
        }
        else {
            survey =
            {
                "SurveyName": SURVEY_NAME,
                "Questions_Answers": {
                    "Intensity": mContext.state.intensity,
                    "WorkoutTime": mContext.state.workoutTime
                },
                "UserId": localStorage.getItem("id")
            };

            helper.put("/Surveys/" + oldSurvey.SurveyId, survey)
                .then(function (value) {
                        mContext.context.router.push("/dashboard");
                    }
                    , function (reason) {
                        alert("Submit survey failed: " + reason.message);
                        console.log("failed update: " + reason.message + reason.error);
                    });
        }
    },
    render: function () {
        return (
            <div className="container">
                <form className="form-signin" onSubmit={this.submitSurvey}>

                    <div>
                        <label htmlFor="day">Day</label>
                        <br/>
                        <select className="drop-down" onChange={this.handleDay}>
                            <option value={0}>Today ({completed[this.state.todayComplete]})</option>
                            <option value={1}>Yesterday ({completed[this.state.yesterdayComplete]})</option>
                        </select>
                    </div>

                    <label htmlFor="workoutTime" className="sr-only">Workout Time (minutes)</label>
                    <input id="workoutTime" className="form-control" value={this.state.workoutTime}
                           onChange={this.handleWorkoutTime} placeholder="Workout Time (minutes)"
                           min="0" max="480" type="number"/>
                    <br/>
                    <label htmlFor="intensity">Practice/Training Session Intensity</label>
                    <input id="intensity" type="range" min="0" max="10" value={this.state.intensity}
                           onChange={this.handleIntensity}/>
                    {this.state.intensity}
                    <div className="row">
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <input type="submit" className="btn btn-lg btn-success btn-block" value="Submit"/>
                        </div>
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <Link to="/dashboard" className="btn btn-lg btn-primary btn-block">Cancel</Link>
                        </div>
                    </div>
                </form>

            </div>

        )
    }
});

module.exports = PostWorkout;