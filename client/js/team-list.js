var React = require('react');

var TeamList = React.createClass({
    render: function() {
        var mContext = this.props.mContext;
        var data = mContext.state.teams;
        var teams = data.map(function(team) {
            return (
                <option key={team.id} value={team.id}>{team.Name}</option>
            )
        });
        return (
            <select value={mContext.state.teamId} onChange={mContext.handleTeam} className="drop-down">
                <option key="" value="">Select a team</option>
                {teams}
            </select>
        );
    }
});

module.exports = TeamList;