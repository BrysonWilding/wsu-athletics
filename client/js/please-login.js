var React = require('react');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;

var PleaseLogin = React.createClass({
    render: function () {
        return (
            <div className="container-fluid">
                <p>Your session has expired, please login</p>
                <p><Link to="/login">
                    <button>OK</button>
                </Link></p>
            </div>
        )
    }
});

module.exports = PleaseLogin;