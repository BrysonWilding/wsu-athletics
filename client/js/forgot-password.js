var React = require('react');
var helper = require('./rest-helper.js');
var ReactRouter = require('react-router');
var Link = ReactRouter.Link;


var ForgotPassword = React.createClass({
    getInitialState: function () {
        return {email: ""};
    },
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    handleEmail: function (e) {
        this.setState({email: e.target.value});
    },
    attemptPasswordReset: function (e) {
        e.preventDefault();
        if(this.state.email.length == 0)
        {
            alert("Email is required");
            return;
        }

        var user = {
            "username": this.state.email
        };
        var mContext = this;

        console.log("email: " + user.username);

        var email = {"email": this.state.email};

        helper.post("/Users/reset", email)
            .then(function (value) {
                    alert("Your new password has been sent to your email.");
                    mContext.context.router.push("/login");
                }
                , function (reason) {
                    alert("Failed to reset password: " + reason.message);
                    console.log("failed: " + reason.message + reason.error);
                });
    },
    render: function () {
        return (
            <div className="container">
                <form className="form-signin" onSubmit={this.attemptPasswordReset}>
                    <h2 className="form-signin-heading">Sucks to suck</h2>
                    <label className="sr-only">Email</label>
                    <input value={this.state.email} onChange={this.handleEmail} type="email" id="email"
                           className="form-control" placeholder="asdf@qwerty.com" required="" autofocus=""/>
                    <br/>
                    <div className="row">
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <input type="submit" className="btn btn-lg btn-success btn-block" value="Submit"/>
                        </div>
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <Link to="/login" className="btn btn-lg btn-primary btn-block">Cancel</Link>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
});

module.exports = ForgotPassword;
